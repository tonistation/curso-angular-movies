import { Injectable } from '@angular/core';
import { User } from '../interface/user'; 
import { DialoginfoComponent } from '../components/users/dialoginfo/dialoginfo.component';
import { DialogpreviewComponent } from '../components/users/dialogpreview/dialogpreview.component';
import { DialogremoveComponent } from '../components/users/dialogremove/dialogremove.component';
import { MatDialog } from '@angular/material'; 
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserRequest } from '../interface/user-request';
import { DialogRequestComponent } from '../components/user-request/dialog-request/dialog-request.component';
import { DialogEvaluateRequestComponent } from '../components/user-request/dialog-evaluate-request/dialog-evaluate-request.component';
import { map } from 'rxjs/operators';// This is where I import map operator
import { UtilsService } from '../services/utils.service';

@Injectable({
  providedIn: 'root' // si no esta aca puede ir en el app module providers
})
export class UserService { 
   
  constructor(
    private afs: AngularFirestore, 
    public dialog: MatDialog,
    private util: UtilsService
  ) {
    //this.headers = new HttpHeaders( { Authorization: localStorage.getItem('token')  } ) 
  }

  list(){
    let usersRef: AngularFirestoreCollection = this.afs.collection('users')
    return usersRef.valueChanges()
  } 

  getUser(userId: string){ 
    let usersRef: AngularFirestoreDocument = this.afs.doc(`users/${userId}`); //this.afs.collection('users', user => user.where('uid','==',userId))
    return usersRef.valueChanges()
  }
  
  listLevels(){ 
    let user_LevelRef: AngularFirestoreCollection = this.afs.collection(`user_level`);
    return user_LevelRef.valueChanges()
  }

  store(data:User){ 
    const usersCollection = this.afs.collection('users');
    usersCollection.add(data)
    return usersCollection.valueChanges()
  }

  update(userId: string, data: any){
    let userRef: AngularFirestoreDocument = this.afs.doc(`users/${userId}`);
    userRef.set(data, { merge: true }) 
    return userRef.valueChanges()
  }

  setUsersRequest(data:UserRequest){ 
    const usersCollection = this.afs.collection('users_request');
    usersCollection.add(data)
    return usersCollection.valueChanges()
  } 

  grantUsersRequest(id){ 
    let userRequestRef: AngularFirestoreDocument = this.afs.doc(`users_request/${id}`);
    let dataGrant = {
      status: 1
    }
    userRequestRef.set(dataGrant, { merge: true }) 
    return userRequestRef.valueChanges()
  }

  checkUserRequest(){
    let dataPendingRequest = []
    this.getUsersRequest().subscribe( (data) => {    
      if(data.length > 0){ 
        this.openDialogEvaluateRequest(data)
      }else{
        this.util.showSB("No hay accesos solicitados","info")
      }
    })
  }

  getUsersRequest(){ 
    let usersRequestRef: AngularFirestoreCollection = this.afs.collection('users_request', ref => ref.where("status", "==", 0))
    return usersRequestRef.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as UserRequest;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  openDialogInfoAccount(name: string): void {
    const dialogRef = this.dialog.open(DialoginfoComponent, {
      width: '300px',
      data: {name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); 
    });
  }

  openDialogAdminRequest(dataRequest: UserRequest): void {  
    const dialogRef = this.dialog.open(DialogRequestComponent, {
      width: '300px',
      data: dataRequest
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); 
    });

    dialogRef.componentInstance.actionOk.subscribe( (data: UserRequest) => {
       this.setUsersRequest(data);
       this.util.showSB("Acceso administrador solicitado", "info")
    });
  }

  openDialogEvaluateRequest(dataRequests: UserRequest[]): void {  
    const dialogRef = this.dialog.open(DialogEvaluateRequestComponent, {
      width: '320px',
      data: dataRequests
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); 
    });

    dialogRef.componentInstance.grantAccessOk.subscribe( id  => {
       this.grantUsersRequest( id ).subscribe( result => {
         if(result.status == 1){
            this.update(result.uid, { level: result.level }).subscribe( resultUU => {
              if(resultUU.level == 3){
                this.util.showSB("Acceso administrador otorgado a " + result.email , "success");
              } 
            }) 
         }
       } )
    });
  }

  openDialogPreview(name: string): void {
    const dialogRef = this.dialog.open(DialogpreviewComponent, {
      width: '400px',
      data: {name: name}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed'); 
    });
  }

  openDialogRemove(user:User): void {
    const dialogRef = this.dialog.open(DialogremoveComponent, {
      width: '400px',
      data: {user: user}
    });

    dialogRef.componentInstance.onAdd.subscribe((u) => {
      // do something
      console.log(u);
      dialogRef.close();
    });
  }
}
