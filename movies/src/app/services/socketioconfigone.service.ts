import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
}) 
 
export class SocketioconfigoneService extends Socket {
    constructor() {
        super({ url: environment.api_socket, options: {} });
    }
}