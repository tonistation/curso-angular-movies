import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesService { 
  constructor(
    private http: HttpClient
  ) { }
 
  list(){
    return this.http.get( `${environment.url_firebase}movies.json` )
  }

  get(movieId){
    return this.http.get( `${environment.url_firebase}movies/${movieId}.json` )
  }

  store(data) {
    return this.http.post(`${environment.url_firebase}movies.json`, data);
  } 

  update(key, data) {
    return this.http.patch(`${environment.url_firebase}movies/${key}.json`, data, {observe: 'response'} );
  } 

  delete(key){
    return this.http.delete(`${environment.url_firebase}movies/${key}.json`, {observe: 'response'} );
  }

  getImdb(idimdb){
    return this.http.get( `${environment.url_imdb}&i=${idimdb}` )
  }
}
