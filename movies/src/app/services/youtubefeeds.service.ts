import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import * as _ from 'lodash'

@Injectable({
  providedIn: 'root'
}) 

export class YoutubefeedsService {

  constructor(
    private http: HttpClient,
    private ngxXml2jsonService: NgxXml2jsonService
  ) { }

  listAdmin(){
    return this.http.get( `${environment.url_firebase}youtube_feeds.json` )
  }

  getInfo(feedId:any){
    return this.http.get( `${environment.url_firebase}youtube_feeds/${feedId}.json` )
  }

  list(callback){
    //console.log('start')
    this.http.get( `${environment.url_firebase}youtube_feeds.json` ).subscribe(
      (list) => {
        let listFeedNews = []
        for (let key in list) { 
          list[key].id = key
          listFeedNews.push( list[key] )
        }
        const parser = new DOMParser();
        const headers = new HttpHeaders({ 'Content-Type': 'text/xml' }).set('Accept', 'text/xml'); 
        

        let number = 0;
        let calls = [];
        for (var _i = 0; _i < listFeedNews.length; _i++) {
          let xmlurl =  'https://cors-anywhere.herokuapp.com/' + environment.rss_youtube + listFeedNews[_i].id_channel
          calls.push(
            this.http.get( xmlurl, { headers: headers, responseType: 'text' } )
          );
        }

        Observable.forkJoin(...calls).subscribe(
          data => {   
            let xmlTemp = '' 
            for (var index = 0; index < data.length; index++) {
              xmlTemp = data[index] 
              const xml = parser.parseFromString( xmlTemp, 'text/xml'); 
              const obj = this.ngxXml2jsonService.xmlToJson(xml);
              //console.log(obj)
              let objxml = _.cloneDeep(obj)
              console.log(objxml)
              listFeedNews[index] = objxml;
              listFeedNews[index].feed.name = listFeedNews[index].feed.title
              
              //console.log(listFeedNews[index].feed.entry)
              for( let key in listFeedNews[index].feed.entry ){ 
                listFeedNews[index].feed.entry[key].image = listFeedNews[index].feed.entry[key]['media:group']['media:thumbnail']['@attributes'].url
                listFeedNews[index].feed.entry[key].description = listFeedNews[index].feed.entry[key]['media:group']['media:description']
                listFeedNews[index].feed.entry[key].videoid = listFeedNews[index].feed.entry[key]['yt:videoId'] 
                listFeedNews[index].feed.entry[key].title = listFeedNews[index].feed.entry[key]['media:group']['media:title']
                listFeedNews[index].feed.entry[key].link = listFeedNews[index].feed.entry[key].link['@attributes'].href
              
                if( typeof listFeedNews[index].feed.entry[key].description !== 'string' )
                  listFeedNews[index].feed.entry[key].description = false;
              }
              
            }
          }, err => console.log('error ' + err),
          () => callback(listFeedNews)
        );
      }
    ) 
  }


  store(data) {
    return this.http.post(`${environment.url_firebase}youtube_feeds.json`, data); 
  } 

  update(key, data) {
    return this.http.patch(`${environment.url_firebase}youtube_feeds/${key}.json`, data, {observe: 'response'} );
  } 

  delete(key){
    return this.http.delete(`${environment.url_firebase}youtube_feeds/${key}.json`, {observe: 'response'} );
  } 
}
