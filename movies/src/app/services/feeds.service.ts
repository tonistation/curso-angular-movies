import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import * as _ from 'lodash'

@Injectable({
  providedIn: 'root' 
})
export class FeedsService {

  constructor(
    private http: HttpClient,
    private ngxXml2jsonService: NgxXml2jsonService
  ) { }

  listAdmin(){
    return this.http.get( `${environment.url_firebase}feeds.json` )
  }

  getInfo(feedId:any){
    return this.http.get( `${environment.url_firebase}feeds/${feedId}.json` )
  }

  list(callback){
    //console.log('start')
    this.http.get( `${environment.url_firebase}feeds.json` ).subscribe(
      (list) => {
        let listFeedNews = []
        for (let key in list) { 
          list[key].id = key
          listFeedNews.push( list[key] )
        }
        const parser = new DOMParser();
        const headers = new HttpHeaders({ 'Content-Type': 'text/xml' }).set('Accept', 'text/xml'); 
        

        let number = 0;
        let calls = [];
        for (var _i = 0; _i < listFeedNews.length; _i++) {
          let xmlurl =  'https://cors-anywhere.herokuapp.com/' + listFeedNews[_i].url
          calls.push(
            this.http.get( xmlurl, { headers: headers, responseType: 'text' } )
          );
        }

        Observable.forkJoin(...calls).subscribe(
          data => {   
            let xmlTemp = '' 
            for (var index = 0; index < data.length; index++) {
              xmlTemp = data[index].replace( /<!\[CDATA\[(.*?)\]\]>/g, '$1' );
              const xml = parser.parseFromString( xmlTemp, 'text/xml');
              const xml2 = data[index];
              const obj = this.ngxXml2jsonService.xmlToJson(xml);
              //console.log(obj)
              let objxml = _.cloneDeep(obj)
              //console.log(objxml)
              if(objxml.hasOwnProperty('rss')){
                listFeedNews[index].feed = objxml;
              }else{
                listFeedNews[index].feed = objxml.rss.channel; 
              }
               
              for( let key in listFeedNews[index].feed.rss.channel.item ){
                //console.log(  listFeedNews[index].feed.rss.channel.item[key] )
                if(  ( listFeedNews[index].feed.rss.channel.item[key].hasOwnProperty('enclosure') ) )
                  listFeedNews[index].feed.rss.channel.item[key].image = listFeedNews[index].feed.rss.channel.item[key].enclosure['@attributes'].url
                else if(  listFeedNews[index].feed.rss.channel.item[key]['media:content'] !== undefined ) 
                  listFeedNews[index].feed.rss.channel.item[key].image = listFeedNews[index].feed.rss.channel.item[key]['media:content']['@attributes'].url
                else if(  listFeedNews[index].feed.rss.channel.item[key]['cartel'] !== undefined ) 
                  listFeedNews[index].feed.rss.channel.item[key].image = listFeedNews[index].feed.rss.channel.item[key]['cartel'].url
              
                if( typeof listFeedNews[index].feed.rss.channel.item[key].description !== 'string' )
                  listFeedNews[index].feed.rss.channel.item[key].description = false;
              }
              
            }
          }, err => console.log('error ' + err),
          () => callback(listFeedNews)
        );
      }
    ) 
  }


  store(data) {
    return this.http.post(`${environment.url_firebase}feeds.json`, data); 
  } 

  update(key, data) {
    return this.http.patch(`${environment.url_firebase}feeds/${key}.json`, data, {observe: 'response'} );
  } 

  delete(key){
    return this.http.delete(`${environment.url_firebase}feeds/${key}.json`, {observe: 'response'} );
  } 
}
