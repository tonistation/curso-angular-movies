import { TestBed } from '@angular/core/testing';

import { SocketioconfigtwoService } from './socketioconfigtwo.service';

describe('SocketioconfigtwoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketioconfigtwoService = TestBed.get(SocketioconfigtwoService);
    expect(service).toBeTruthy();
  });
});
