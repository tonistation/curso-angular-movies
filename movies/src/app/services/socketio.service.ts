import { Injectable } from '@angular/core';  
import { SocketioconfigoneService } from '../services/socketioconfigone.service'  

@Injectable({
  providedIn: 'root'
})
export class SocketioService{

  constructor(
    private socket: SocketioconfigoneService,
  ) { }

  // Emitir 
  emit( eventName:string, params?:any ) { 
    this.socket.emit(eventName, params)
  }

  // listen o escucha
  on( eventName:string, callback) {
    this.socket.on(eventName, callback)
  }
 
}

  
