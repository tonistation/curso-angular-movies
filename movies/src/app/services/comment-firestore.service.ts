import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CommentFirestoreService {
  
  commentsCollection: AngularFirestoreCollection
  constructor(
    private db:AngularFirestore,
    private auth:AuthService
  ) {
    this.commentsCollection = this.db.collection('comments')
   }

  load(){
    return this.commentsCollection.valueChanges()
  }

  addComment( texto:string ){
    const data:any = this.auth.getInfo
    const comment = {
      message: texto,
      name: data.nombres,
      fecha: new Date()
    }
    this.commentsCollection.add(comment)
  }
}
