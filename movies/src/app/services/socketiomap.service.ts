import { Injectable } from '@angular/core';  
import { SocketioconfigtwoService } from '../services/socketioconfigtwo.service'  

@Injectable({
  providedIn: 'root'
})
export class SocketiomapService{

  constructor(
    private socket: SocketioconfigtwoService
  ) { }

  // Emitir
  emit( eventName:string, params?:any ) {
    this.socket.emit(eventName, params)
  }

  // listen o escucha
  on( eventName:string, callback) {
    this.socket.on(eventName, callback)
  }
 
}
