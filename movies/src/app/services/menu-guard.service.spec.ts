import { TestBed } from '@angular/core/testing';

import { MenuGuardService } from './menu-guard.service';

describe('MenuGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuGuardService = TestBed.get(MenuGuardService);
    expect(service).toBeTruthy();
  });
});
