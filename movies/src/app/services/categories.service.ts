import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {   
  constructor(
    private http: HttpClient
  ) { }
 
  list(){
    return this.http.get( `${environment.url_firebase}categories.json` )
  }

  get(categoryId){
    return this.http.get( `${environment.url_firebase}categories/${categoryId}.json` )
  }

  store(data) {
    return this.http.post(`${environment.url_firebase}categories.json`, data);
  } 

  update(key, data) {
    return this.http.patch(`${environment.url_firebase}categories/${key}.json`, data);
  } 

  delete(key){
    return this.http.delete(`${environment.url_firebase}categories/${key}.json`, {observe: 'response'} );
  }
}

