import { TestBed } from '@angular/core/testing';

import { CommentFirestoreService } from './comment-firestore.service';

describe('CommentFirestoreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommentFirestoreService = TestBed.get(CommentFirestoreService);
    expect(service).toBeTruthy();
  });
});
