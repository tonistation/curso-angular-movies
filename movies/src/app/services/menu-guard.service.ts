import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class MenuGuardService implements CanActivate {

  constructor(
    private auth:AuthService ,
    private router: Router
  ) { }

  canActivate(act:ActivatedRouteSnapshot, state: RouterStateSnapshot){
    const thePath = act.routeConfig.path

    /*console.log(act) 
    console.log(act.routeConfig.data.secure) 
    console.log(act.url.toString()) 
    console.log(state) 
    console.log(state.url) 
    const theUrl = state.url */
    
    const sesdata = this.auth.getInfo 

    if( this.auth.isLoggedIn ){ 
      return (sesdata.level === 3)
    }else{
      return this.router.parseUrl("/login");
    } 
    
  }
}
