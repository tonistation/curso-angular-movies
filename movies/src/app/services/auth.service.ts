import { Injectable, HostBinding } from '@angular/core'; 
import { JwtHelperService } from '@auth0/angular-jwt'
import { auth } from 'firebase/app';
import { Router } from  "@angular/router";  
import { AngularFireAuth } from  "@angular/fire/auth";  
import { AngularFirestoreDocument, AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UtilsService } from './utils.service'
import { DialogwelcomeComponent } from '../components/shared/dialogwelcome/dialogwelcome.component' 
import { MatDialog } from '@angular/material';
import { User } from '../interface/user'

@Injectable({
  providedIn: 'root'
})
export class AuthService { 
  user: any
  
  @HostBinding('class') componentCssClass;

  constructor(  
    public  afAuth:  AngularFireAuth, 
    public  router:  Router,
    private afs: AngularFirestore,
    private utilsService: UtilsService,
    private dialog: MatDialog 
  ) {

    this.afAuth.authState.subscribe(user => { 
      if (user) {
        this.user = user;   
        this.getExtraInfo() 
      } else {
        localStorage.setItem('userWWM', null);        
        //this.utilsService.showSB(`${ name }, Te esperamos de vuelta)`, 'info')
      }
    })
    
  }

  getExtraInfo(){
    let usersRef:  AngularFirestoreDocument = this.afs.doc(`users/${this.user.uid}`)
    usersRef.valueChanges().subscribe( 
      (data)=> {     
        console.log(data.displayName)

        if(data.access !== undefined && data.access) {
          let datauser:any = [ 
            'ra'
           ].reduce((result, key) => { result[key] = this.user[key]; return result; }, {});  

          const name = (data.displayName !== null) ? this.utilsService.getFirstWord( data.displayName ) : data.email;

          datauser.extra = { ...data, ...this.user.providerData[0], name: name, uid: data.uid } 

          this.utilsService.showSB(`Hola ${ name }, Bienvenido(a)`, 'info')
          if(datauser.extra.theme === undefined || datauser.extra.bgmodel === undefined){ 
            datauser.extra.theme = (datauser.extra.theme === undefined) ? 'light-theme' : datauser.extra.theme;
            datauser.extra.bgmodel = (datauser.extra.bgmodel === undefined) ? 'model1' : datauser.extra.bgmodel;
            localStorage.setItem('userWWM', JSON.stringify(datauser));  
            this.showDialogWelcome()  
          }    
          document.getElementsByTagName('body')[0].classList.value = datauser.extra.theme;
          this.componentCssClass = datauser.extra.theme; 
          document.getElementsByTagName('body')[0].setAttribute("bgmodel", datauser.extra.bgmodel); 
          localStorage.setItem('userWWM', JSON.stringify(datauser));  
        }else{
          this.utilsService.showSB(`Lo sentimos, esta cuenta ha sido bloqueada por un administrador`, 'error')
          this.logout()
        }        
      }
    )
  }

  login( credentials:any ) {
    return this.afAuth.auth.signInWithEmailAndPassword( credentials.username, credentials.password )
  }

  
  loginNormalSuccess(res) { 
    this.updateUserData(res.user); 
  }
 
  loginGoogle() { 
    this.doGoogleLogin()
      .then(res =>{
        console.log(res)
        this.updateUserData(res.user);
     }, err => console.log(err)
    )
  }

  doGoogleLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  loginFacebook() { 
    this.doFacebookLogin()
      .then(res =>{
        console.log(res)
        this.updateUserData(res.user);
      }, err => console.log(err)
    )
  }

  doFacebookLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  loginTwitter() { 
    this.doTwitterLogin()
      .then(res =>{
        console.log(res)
        this.updateUserData(res.user);
      }, err => console.log(err)
    )
  }

  doTwitterLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new auth.TwitterAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
  }

  private updateUserData(user) { 
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument = this.afs.doc(`users/${user.uid}`) 

    userRef.valueChanges().subscribe(
      ( dataResponse ) => {  
        const data = { 
          uid: user.uid,  
          email: user.email, 
          displayName: user.displayName, 
          photoURL: user.photoURL,
          access: ( dataResponse === undefined || dataResponse.access === true ) ? true : false,
          level: ( dataResponse === undefined || dataResponse.level === undefined ) ? 1 : user.level
        }    
        return userRef.set(data, { merge: true }) 
      } 
    )  
  }

  logout(){
    this.afAuth.auth.signOut();
    localStorage.removeItem('userWWM');
    this.router.navigate(['/']);  
  } 

 /* login(credentials){
    this.headers = new HttpHeaders({ Authorization: `Basic ${btoa(credentials.username+':'+credentials.password)}` })
    return this.http.post(`${environment.api_url}auth/usuarios`, {}, { headers: this.headers }).toPromise()
  }*/
  

 /* isLogged(){
    const token = localStorage.getItem('token')
    return (token) ? true : false
  }*/

  get isLoggedIn(): boolean {
    const  user  =  JSON.parse(localStorage.getItem('userWWM'));
    return  user  !==  null;
  }

 /* logout(){
    localStorage.removeItem('token')
  }*/
 

  get getInfo() {
    /*const token = localStorage.getItem('token') */ 
    //const JwtHelper: JwtHelperService = new JwtHelperService()
    //JwtHelper.decodeToken(user.ra) 

    const data: any = JSON.parse( localStorage.getItem('userWWM') ).extra
    return data
  } 

  showDialogWelcome(){
    const datauser = this.getInfo
    const dw = this.dialog.open(DialogwelcomeComponent, {
      width: 'initial',
      data: {user: datauser},
      disableClose: true,
      panelClass: 'bg-neutro'
    }); 

    dw.componentInstance.selectTheme.subscribe((theme:any) => { 
      this.updateTheme = theme
      dw.close();  
    });  

    dw.componentInstance.selectBgModel.subscribe((bgmodel:any) => { 
      this.updateBgModel = bgmodel
      dw.close(); 
    });  
  }

  set updateTheme(theme: string){
    document.getElementsByTagName('body')[0].classList.value = theme;
    this.componentCssClass = theme;
    const userinfo = this.getInfo 
    const userRef: AngularFirestoreDocument = this.afs.doc(`users/${userinfo.uid}`);
    const data = { 
      theme: theme
    }  
    userRef.set(data, { merge: true })
  } 

  set updateBgModel(bgmodel: string){
    document.getElementsByTagName('body')[0].setAttribute("bgmodel", bgmodel); 
    const userinfo = this.getInfo 
    const userRef: AngularFirestoreDocument = this.afs.doc(`users/${userinfo.uid}`);
    const data = { 
      bgmodel: bgmodel
    }  
    userRef.set(data, { merge: true })
  }
}
