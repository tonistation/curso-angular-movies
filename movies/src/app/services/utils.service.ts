import { Injectable } from '@angular/core';
import { SnackbarComponent } from '../components/shared/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay'; 

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private snackBar: MatSnackBar
  ) {
    
  }

  showSB(message: string, type:string, duration:number = 3000) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: duration,
      data: { message: message, type: type }
    });
  } 

  getFirstWord(text:string){
    return text.replace(/ .*/,'')
  }
}
