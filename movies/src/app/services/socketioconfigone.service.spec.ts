import { TestBed } from '@angular/core/testing';

import { SocketioconfigoneService } from './socketioconfigone.service';

describe('SocketioconfigoneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketioconfigoneService = TestBed.get(SocketioconfigoneService);
    expect(service).toBeTruthy();
  });
});
