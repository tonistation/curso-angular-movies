import { TestBed } from '@angular/core/testing';

import { SocketiomapService } from './socketiomap.service';

describe('SocketiomapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketiomapService = TestBed.get(SocketiomapService);
    expect(service).toBeTruthy();
  });
});
