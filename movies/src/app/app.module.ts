import { BrowserModule } from '@angular/platform-browser';
import { Injectable, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { MenuComponent } from './components/shared/menu/menu.component';
import { DirectivasComponent } from './components/directivas/directivas.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { FormUserComponent } from './components/users/form-user/form-user.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { SocketIoModule } from 'ng-socket-io'   
import { AngularFireModule } from '@angular/fire'
import { AngularFireStorageModule, StorageBucket} from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ImageCompressService,ResizeOptions,ImageUtilityService } from 'ng2-image-compress';
import { TimeagoModule } from 'ngx-timeago';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxPaginationModule } from 'ngx-pagination'; 
import { EmbedVideo } from 'ngx-embed-video';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';  
import { fab } from '@fortawesome/free-brands-svg-icons';


import { 
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule, 
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule, 
  MatSnackBarModule,
  MatStepperModule,
  MatSortModule, 
  MatTableModule, 
  MatToolbarModule,
  MatTooltipModule, 
  MatIconModule, 
} from '@angular/material';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { DialoginfoComponent } from './components/users/dialoginfo/dialoginfo.component';
import { DialogpreviewComponent } from './components/users/dialogpreview/dialogpreview.component';
import { DialogremoveComponent } from './components/users/dialogremove/dialogremove.component'; 
import { ChatComponent } from './components/chat/chat.component';
import { MapsComponent } from './components/maps/maps.component';
import { ChartAtPipe } from './pipes/chart-at.pipe';
import { ListCategoriesComponent } from './components/categories/list-categories/list-categories.component';
import { ReadfirebasePipe } from './pipes/readfirebase.pipe';
import { FormCategoriesComponent } from './components/categories/form-categories/form-categories.component';
import { DialogremoveCategoryComponent } from './components/categories/dialogremove-category/dialogremove-category.component';
import { ListMoviesComponent } from './components/movies/list-movies/list-movies.component';
import { FormMoviesComponent } from './components/movies/form-movies/form-movies.component';
import { DialogremoveMoviesComponent } from './components/movies/dialogremove-movies/dialogremove-movies.component';
import { environment } from 'src/environments/environment.prod';
import { CardComponent } from './components/shared/card/card.component';
import { SnackbarComponent } from './components/shared/snackbar/snackbar.component';
import { TablelistComponent } from './components/shared/tablelist/tablelist.component';
import { LightboxComponent } from './components/shared/lightbox/lightbox.component';
import { LoginComponent } from './components/login/login.component'; 
import { ListCommentsComponent } from './components/comments/list-comments/list-comments.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ListFeedsComponent } from './components/feeds/list-feeds/list-feeds.component';
import { ListMoviesFeedsComponent } from './components/feeds/list-movies-feeds/list-movies-feeds.component';
import { DialogwelcomeComponent } from './components/shared/dialogwelcome/dialogwelcome.component';
import { AdminFeedsComponent } from './components/feeds/admin-feeds/admin-feeds.component'; 
import { ShowFeedComponent } from './components/feeds/show-feed/show-feed.component';
import { ImagecheckPipe } from './pipes/imagecheck.pipe';
import { RemovebrPipe } from './pipes/removebr.pipe';
import { ViewAdminFeedComponent } from './components/feeds/view-admin-feed/view-admin-feed.component';
import { FormAdminFeedComponent } from './components/feeds/form-admin-feed/form-admin-feed.component';
import { DialogremoveFeedsComponent } from './components/feeds/dialogremove-feeds/dialogremove-feeds.component';
import { ListYoutubefeedsComponent } from './components/feeds/list-youtubefeeds/list-youtubefeeds.component';
import { DialogvideoComponent } from './components/feeds/dialogvideo/dialogvideo.component';
import { ViewUserComponent } from './components/users/view-user/view-user.component';
import { ViewMoviesComponent } from './components/movies/view-movies/view-movies.component';
import { ChatMoviesComponent } from './components/movies/chat-movies/chat-movies.component';
import { UserRequestComponent } from './components/user-request/user-request.component';
import { DialogRequestComponent } from './components/user-request/dialog-request/dialog-request.component';
import { DialogEvaluateRequestComponent } from './components/user-request/dialog-evaluate-request/dialog-evaluate-request.component';

@NgModule({
  // componentes
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    DirectivasComponent,
    ListUsersComponent,
    FormUserComponent,
    DashboardComponent,
    DialoginfoComponent,
    DialogpreviewComponent,
    DialogremoveComponent,
    ChatComponent,
    MapsComponent,
    ChartAtPipe,
    ListCategoriesComponent,
    ReadfirebasePipe,
    FormCategoriesComponent,
    DialogremoveCategoryComponent,
    ListMoviesComponent,
    FormMoviesComponent,
    DialogremoveMoviesComponent,
    CardComponent,
    SnackbarComponent,
    TablelistComponent, LightboxComponent, LoginComponent, ListCommentsComponent, ListFeedsComponent, ListMoviesFeedsComponent, DialogwelcomeComponent, AdminFeedsComponent, ShowFeedComponent, ImagecheckPipe, RemovebrPipe, ViewAdminFeedComponent, FormAdminFeedComponent, DialogremoveFeedsComponent, ListYoutubefeedsComponent, DialogvideoComponent, ViewUserComponent, ViewMoviesComponent, ChatMoviesComponent, UserRequestComponent, DialogRequestComponent, DialogEvaluateRequestComponent
  ],
  // modulos o librerias
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,  
    FontAwesomeModule,
    NgxPaginationModule,
    EmbedVideo,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule, 
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule, 
    MatSnackBarModule,
    MatSortModule, 
    MatTableModule, 
    MatToolbarModule,
    MatTooltipModule,
    MatIconModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    SocketIoModule,
    MatFileUploadModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    TimeagoModule.forRoot()
  ],
  // servicios
  providers: [
    { provide: StorageBucket, useValue: environment.firebase.storageBucket }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialoginfoComponent,
    DialogpreviewComponent,
    DialogremoveComponent,
    DialogremoveCategoryComponent,
    DialogremoveMoviesComponent,
    DialogwelcomeComponent,
    DialogremoveFeedsComponent,
    SnackbarComponent,
    LightboxComponent,
    DialogvideoComponent,
    ChatMoviesComponent,
    DialogRequestComponent,
    DialogEvaluateRequestComponent
  ],
}) 

export class AppModule {
  constructor() { 
    library.add(fas, far, fab );
  }
}


