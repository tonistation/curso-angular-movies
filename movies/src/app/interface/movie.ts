export interface Movie {
    name: string,
    id?:string,
    image?: string,
    idimdb?: string,
    categories_id?: string,
    description?:string,
    categories_name?: string
}
