export interface Feed {
    name: string,
    id?:string,  
    description?:string,
    url: string,
    item?: []
}
