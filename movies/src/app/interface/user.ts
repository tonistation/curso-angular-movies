export interface User {
    id?: string
    uid?:string,
    image?: string,
    photoURL: string,
    displayName: string,
    providerId?: string,
    email: string, 
    admin?:boolean,
    bgmodel:string,
    theme:string,
    name?:string,
    level?:number
}
