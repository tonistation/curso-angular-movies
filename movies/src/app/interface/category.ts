export interface Category {
    id?:string,
    name:string,
    description?:string,
    total_movies?:number,
    movies?:any
}
