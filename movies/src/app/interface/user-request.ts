export interface UserRequest {
    email: string,
    uid: string,
    date: Date,
    level: number,
    status: number,
    id?: string
}
