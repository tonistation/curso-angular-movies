import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectivasComponent } from './components/directivas/directivas.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { FormUserComponent } from './components/users/form-user/form-user.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChatComponent } from './components/chat/chat.component';
import { MapsComponent } from './components/maps/maps.component';
import { ListCategoriesComponent } from './components/categories/list-categories/list-categories.component';
import { FormCategoriesComponent } from './components/categories/form-categories/form-categories.component';
import { ListMoviesComponent } from './components/movies/list-movies/list-movies.component';
import { FormMoviesComponent } from './components/movies/form-movies/form-movies.component';
import { MenuGuardService } from './services/menu-guard.service';
import { ListCommentsComponent } from './components/comments/list-comments/list-comments.component';
import { LoginComponent } from './components/login/login.component';
import { ListFeedsComponent } from './components/feeds/list-feeds/list-feeds.component';
import { ListMoviesFeedsComponent } from './components/feeds/list-movies-feeds/list-movies-feeds.component'; 
import { AdminFeedsComponent } from './components/feeds/admin-feeds/admin-feeds.component';
import { ShowFeedComponent } from './components/feeds/show-feed/show-feed.component';
import { ViewAdminFeedComponent } from './components/feeds/view-admin-feed/view-admin-feed.component';
import { FormAdminFeedComponent } from './components/feeds/form-admin-feed/form-admin-feed.component';
import { ListYoutubefeedsComponent } from './components/feeds/list-youtubefeeds/list-youtubefeeds.component';
import { ViewUserComponent } from './components/users/view-user/view-user.component';
import { ViewMoviesComponent } from './components/movies/view-movies/view-movies.component';
import { UserRequestComponent } from './components/user-request/user-request.component';

const routes: Routes = [
  { 
    path: '', 
    component: DashboardComponent 
  },
  { 
    path: 'login',
    component: LoginComponent
  },
  { 
    path: 'directivas', 
    component: DirectivasComponent 
  },
  { 
    path: 'users',  
    children: [
        { path:'', component:ListUsersComponent },
        { path:'list', component:ListUsersComponent, canActivate: [MenuGuardService] },
        { path:'new', component:FormUserComponent, canActivate: [MenuGuardService]  },
        { path:':userId/view', component:ViewUserComponent, canActivate: [MenuGuardService]  },
        { path:':userId/edit', component:FormUserComponent, canActivate: [MenuGuardService]  },
        { path:'request', component:UserRequestComponent, canActivate: [MenuGuardService]  }
      ],
      canActivate: [MenuGuardService]
  },
  { 
    path: 'feeds',  
    children: [
        { path:'', component:ListFeedsComponent },
        { path:'news', component:ListFeedsComponent },
        { path:'videos', component:ListYoutubefeedsComponent  },
        { path:'admin', component:AdminFeedsComponent, canActivate: [MenuGuardService] }, 
        { path:'admin/new', component:FormAdminFeedComponent, canActivate: [MenuGuardService]},
        { path:':feedId/view', component: ViewAdminFeedComponent, canActivate: [MenuGuardService] } ,
        { path:':feedId/edit', component: FormAdminFeedComponent, canActivate: [MenuGuardService] } ,
      ]
  },
  { 
    path: 'chat', 
    children: [
        { path:'', component:ChatComponent  }
      ]
  },
  { 
    path: 'maps', 
    children: [
        { path:'', component:MapsComponent  }
      ]
  },
  { 
    path: 'categories', 
    children: [
        { path:'', component:ListCategoriesComponent  },
        { path:'list', component:ListCategoriesComponent  },
        { path:'new', component:FormCategoriesComponent  },
        { path:':categoryId/edit', component:FormCategoriesComponent  }
      ]
  },
  { 
    path: 'movies', 
    children: [
        { path:'', component:ListMoviesComponent  },
        { path:'list', component:ListMoviesComponent  },
        { path:'new', component:FormMoviesComponent  },
        { path:':movieId/edit', component:FormMoviesComponent  },
        { path:':movieId/view', component:ViewMoviesComponent  }
      ]
  },
  { 
    path: 'comments', 
    children: [
        { path:'', component:ListCommentsComponent  },
        { path:'list', component:ListCommentsComponent  } 
      ]
  },
  { 
    path: '**', 
    redirectTo: '' 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
