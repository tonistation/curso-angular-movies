import { ChangeDetectorRef, Component, ViewChild, HostListener, HostBinding } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { SidenavService } from './service/shared/sidenav.service';
import { MatSidenav } from '@angular/material';
import { AuthService } from './services/auth.service';
import { Router } from  "@angular/router";
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Worl Movies';

  mobileQuery: MediaQueryList; 
  @ViewChild('snav') public sidenav: MatSidenav; 

  @HostListener('window:resize', ['$event'])
  onResize(event) {
      if (event.target.innerWidth < 768) {
        this.sidenavService.close()
      }
  }
  @HostBinding('class') componentCssClass;

  private _mobileQueryListener: () => void;

  constructor(
    public auth:AuthService,
    private changeDetectorRef: ChangeDetectorRef, 
    media: MediaMatcher,
    private sidenavService: SidenavService,
    public  router:  Router,
    public overlayContainer: OverlayContainer
    ) 
  {
    if(!this.auth.isLoggedIn)
      this.router.navigate(['/login']);  

    this.mobileQuery = media.matchMedia('(max-width: 600px)'); 
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener); 
  }

  ngOnInit(): void {  
    this.sidenavService.setSidenav(this.sidenav);  
    this.changeDetectorRef.detectChanges();  
    //this.onSetTheme('dark-theme')
  }
  
  // light-theme o dark-theme
  onSetTheme(theme) { 
    
    this.overlayContainer.getContainerElement().classList.add(theme);
    this.componentCssClass = theme;
  }
}

