import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removebr'
})
export class RemovebrPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const lastC = value.substr( value.length -10 );
    const lastC2 =value.substr( value.length -5 );

    if(lastC == '<br/><br/>')
      return value.substr( 0, value.length -10)
    
    if(lastC2 == '<br/>')
      return value.substr( 0, value.length -5)

    return value;
  }

}
