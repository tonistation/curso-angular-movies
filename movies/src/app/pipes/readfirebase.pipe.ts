import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'readfirebase'
})
export class ReadfirebasePipe implements PipeTransform {

  transform(value: any, args?: any) { 
    //console.log(value);
    if(value !== undefined){
      const result = []
      if(value.length !== 0){ 
        for (let key in value) {
          //console.log(' name=' + key + ' value=' + value[key]); 
          value[key].id = key
          result.push( value[key] )
        }
      } 
     // console.log(result);
      return result;
    }else{
      return value;
    }
   
  }

}
