import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chartAt'
})
export class ChartAtPipe implements PipeTransform {
  
  transform(value: any, args?: any): any {
    let lastLetter: string;
    let firstLetters: string;
    if( args === 1){ 
      firstLetters = value.substr( 1, value.length - 1 )
      lastLetter = value.charAt( 0 ).toUpperCase()
    }else if( args === 2){ 
      firstLetters = value.substr( 0, value.length - 1 )
      lastLetter = value.charAt( value.length - 1  ).toUpperCase()
    } 
    return `${firstLetters}${lastLetter}` 
  }

}
