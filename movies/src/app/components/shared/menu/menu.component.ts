import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service'; 
import { User } from '../../../interface/user' 
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  sesdata: User
  menu_open = ''
  menu_children_open = ''
  menu_list: any[]
  theme_select: any
  photoUser: string
  
  constructor(
    public auth:AuthService
  ) { 
    this.sesdata = this.auth.getInfo 
    this.photoUser = (this.sesdata.photoURL) ? this.sesdata.photoURL : environment.photoURL
    this.theme_select = this.sesdata['colormode'] + '-theme'
    this.theme_select = 'white-theme'
  }
 
  ngOnInit() { 

    let menuAdmin = [
      { icon:'group', item:'users', url:'/users', title: 'Usuarios', childrens: 
        [
          { icon:'view_list', item:'users_list', url:'/users/list', title: 'Listado'  },
          { icon:'person_add', item:'users_add', url:'/users/new', title: 'Crear'  },
          { icon:'verified_user', item:'users_permissions', url:'/users/permisos', title: 'Permisos'  }
        ] 
      },
    ]
     
    this.menu_list = [
      { icon:'chrome_reader_mode', item:'feeds', url:'/feeds', title: 'Noticias', childrens: 
        [
          { icon:'local_library', item:'feeds_news', url:'/feeds/news', title: 'Fuentes RRSS'  },
          { icon:'ondemand_video', item:'feeds_videos', url:'/feeds/videos', title: 'Youtubers'  },
          { icon:'rss_feed', item:'feeds_admin', url:'/feeds/admin', title: 'Administrar Fuentes'  }  
        ] 
      }, 
      { icon:'movie', item:'movies', url:'/movies', title: 'Peliculas', childrens: 
        [
          { icon:'view_list', item:'movies_list', url:'/movies/list', title: 'Listado'  },
          { icon:'add_circle_outline', item:'movies_add', url:'/movies/new', title: 'Crear'  }
        ] 
      },
      { icon:'group_work', item:'categories', url:'/categories', title: 'Categorias', childrens: 
        [
          { icon:'view_list', item:'categories_list', url:'/categories/list', title: 'Listado'  },
          { icon:'add_circle_outline', item:'categories_add', url:'/categories/new', title: 'Crear'  }
        ] 
      }, 
    ]

    if(this.sesdata.level === 3){
      for( let ma of menuAdmin ){ 
        this.menu_list.push(ma)
      }
    }
  }

}
