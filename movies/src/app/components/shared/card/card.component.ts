import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})

export class CardComponent implements OnInit {
  @Input() item: any;
  @Input() colorMode: string;
  @Input() type_item: string;
  @Input() icon: string;
  @Output() remove = new EventEmitter();
  @Output() openLightBox = new EventEmitter(); 

  itemsSHowButtons = []
  itemsShowDeleteButton = []
  showDelete: boolean = true

  constructor() { } 

  ngOnInit() {
    this.itemsSHowButtons = ["movies", "categories", "feeds", "users"] 
    this.itemsShowDeleteButton = ["movies", "user"] 
  }

  openLB(index){ 
    this.openLightBox.emit(index)
  }

  removeChild(item){
    this.remove.emit(item)
  } 

  errorImg(){
    this.item.image = undefined;
  }

  get showButtonBar(){
    return this.itemsSHowButtons.indexOf(this.type_item) > -1;
  }

  get showDeleteButton(){
    return this.itemsShowDeleteButton.indexOf(this.type_item) > -1;
  }

}
