import { Component, OnInit, Input} from '@angular/core';   
import { SidenavService } from '../../../service/shared/sidenav.service';
import { UserService } from '../../../service/user.service';
import { AuthService } from 'src/app/services/auth.service'; 
import { MatSidenav } from '@angular/material'; 
import { UserRequest } from 'src/app/interface/user-request';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})


export class HeaderComponent implements OnInit {
  @Input() sidenav: MatSidenav;
  name:string = 'Jose Rivas'
  public dataUser: any
  
  constructor(
    public auth: AuthService,
    private sidenavService: SidenavService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.sidenavService.setSidenav(this.sidenav); 
    this.dataUser = this.auth.getInfo 
    if(this.dataUser.level === 3)
      this.getUsersRequest()
  }

  changeStyle() {
    this.auth.showDialogWelcome()
  }

  toggleSidenav() {
    this.sidenav.toggle();
  } 

  openDialogInfoAccount(): void {
    this.userService.openDialogInfoAccount('Jose')
  } 

  openDialogAdminRequest(): void {
    const dataRequest: UserRequest = {
      email: this.dataUser.email,
      uid: this.dataUser.uid,
      level: 3,
      status: 0,
      date: new Date()
    }
    this.userService.openDialogAdminRequest(dataRequest)
  }
 
  getUsersRequest() {
    this.userService.checkUserRequest();
  }
}