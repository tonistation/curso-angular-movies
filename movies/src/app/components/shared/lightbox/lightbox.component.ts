import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.component.html',
  styleUrls: ['./lightbox.component.css']
})
export class LightboxComponent {
  selectMedia: any = {}
  constructor(
    public dialogRef: MatDialogRef<LightboxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { 
  }
 
  open(i){
    this.selectMedia = this.data.album[i] 
  }

  ngOnInit() { 
    this.open(this.data.index)
  }

}


export interface DialogData { 
  name: string,
  index: number,
  album: any
}
