import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogwelcomeComponent } from './dialogwelcome.component';

describe('DialogwelcomeComponent', () => {
  let component: DialogwelcomeComponent;
  let fixture: ComponentFixture<DialogwelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogwelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogwelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
