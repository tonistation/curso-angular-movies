
import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';  

@Component({
  selector: 'app-dialogwelcome',
  templateUrl: './dialogwelcome.component.html',
  styleUrls: ['./dialogwelcome.component.css']
})
export class DialogwelcomeComponent {
  selectTheme = new EventEmitter();
  selectBgModel = new EventEmitter();

  constructor(
    public dialogRef: MatDialogRef<DialogwelcomeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData 
  ) { }

  setTheme(theme: string){
    this.selectTheme.emit(theme)
  }

  setBgModel(bgmodel: string){
    this.selectBgModel.emit(bgmodel)
  }

} 

export interface DialogData { 
  user: any;
}