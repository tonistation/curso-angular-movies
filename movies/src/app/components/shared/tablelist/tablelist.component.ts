import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tablelist',
  templateUrl: './tablelist.component.html',
  styleUrls: ['./tablelist.component.css']
})

export class TablelistComponent implements OnInit {
  @Input() data: any;
  @Input() type_item: string;
  @Input() colorMode: string; 
  @Output() remove = new EventEmitter();
  @Output() openLightBox = new EventEmitter(); 

  constructor() { }

  ngOnInit() {
  }

  removeChild(item){
    this.remove.emit(item)
  }
 

}
