import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMoviesComponent } from './chat-movies.component';

describe('ChatMoviesComponent', () => {
  let component: ChatMoviesComponent;
  let fixture: ComponentFixture<ChatMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
