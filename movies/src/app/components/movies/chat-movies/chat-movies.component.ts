import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core'; 
import { SocketioService } from 'src/app/services/socketio.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service'; 
import { Router, Event, NavigationStart } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { ReadfirebasePipe } from 'src/app/pipes/readfirebase.pipe';
import * as _ from 'lodash'


@Component({
  selector: 'app-chat-movies',
  templateUrl: './chat-movies.component.html', 
  styleUrls: ['./chat-movies.component.css'],
  providers: [ ReadfirebasePipe ]
})
export class ChatMoviesComponent implements OnInit {
  @Input() type_item: any;
  @Input() chatId: any; 
  @ViewChild('textBox') textBoxElement: ElementRef;

  mode: number;
  newMsg: string;
  chat_list: any[];
  dataChat: {
    username: string,
    displayName: string,
    room: string,
    msg: string,
    image?: string
  };
  msgBox: string; 
  isJoined: boolean = false;
  username: string;
  isOpen: boolean = false;
  showErrorConnect: boolean = false;
  dataUser: any

  constructor(
    public sio: SocketioService,
    private route: ActivatedRoute,
    public auth: AuthService,
    private router: Router, 
    private utilService: UtilsService,
    private fbPipe: ReadfirebasePipe
  ) {  }

  ngOnInit() {  
    this.mode = 1; 
    this.dataUser = this.auth.getInfo  
    this.username = this.dataUser.email;
    this.dataChat = {
      username:  this.dataUser.email,
      displayName:  (this.dataUser.displayName !== null) ? this.dataUser.displayName : this.dataUser.email,
      room: this.chatId,  
      msg: ''
    } 
    
    const source = this.sio.on('is_online',  (data) => {   
      if(data.username === this.dataUser.email){
        this.isJoined = true;
        if(data.data !== undefined && data.data !== null){  
          for (let key in data.data) { 
              data.data[key].type = 0;
          } 
          this.chat_list = this.fbPipe.transform(data.data);
        } 
      } 
    });  

    const eventMsg = this.sio.on('chat_message',  (data: any) => { 
      if(this.isJoined && data.msg.username !== this.dataUser.email){ 
        this.playAudio();
      }  
      data.msg.type = 1; 
      this.chat_list.push(data.msg);
    });
    
    const eventError = this.sio.on('connect_error',  (data) => {
      this.isOpen = false;
      this.isJoined = false;
      if(!this.showErrorConnect){
        this.showErrorConnect = true;
        this.utilService.showSB("Error conectando al servicio de chat, lo sentimos :( ", "error")
      }        
    });

    const eventLogout = this.sio.on('logout',  (data) => {  
      if(data.msg.username === this.dataUser.email){
        this.isJoined = false;
      }
      data.msg.type = 0; 
      this.chat_list.push(data.msg);
    });  

    this.router.events.subscribe( (event: Event) => {
        if (event instanceof NavigationStart) {  
          //this.sio.emit('disconnect', this.dataChat);
        } 
      }
    )
  } 

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/audio/notification.mp3";
    audio.load();
    audio.play();
  }

  joinChat(){
    this.isOpen = true;
    this.showErrorConnect = false;
    this.sio.emit('join', this.dataChat);
  }

  hasProp (obj, prop) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  } 
  submit(chatId) {
    /*this.cs.sendMessage(chatId, this.newMsg);
    this.newMsg = '';*/
  }

  trackByCreated(i, msg) {
    return msg.createdAt;
  }
  
  sendMsg($event){
    this.dataChat.msg = this.msgBox; 
    this.dataChat.image = this.dataUser.photoURL
    this.msgBox = "";
    setTimeout(()=>{
      this.textBoxElement.nativeElement.value = '';
      this.textBoxElement.nativeElement.focus();
    },0);  
    this.sio.emit('chat_message', this.dataChat);
  }

  logoutChat(){
    this.isOpen = false; 
    this.sio.emit('logout', this.dataChat);
  }

}
