import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router' 
import { Movie } from '../../../interface/movie'
import { Category } from '../../../interface/category'
import { CategoriesService } from '../../../services/categories.service'
import { MoviesService } from '../../../services/movies.service'
import { AngularFireStorage } from '@angular/fire/storage';
import { environment } from 'src/environments/environment.prod';
import { ImageCompressService } from  'ng2-image-compress';


@Component({
  selector: 'app-form-movies',
  templateUrl: './form-movies.component.html',
  styleUrls: ['./form-movies.component.css']
})

export class FormMoviesComponent implements OnInit {
  isLoadingResults:boolean = false
  movie: Movie = {
    name: '',
    description: '',
  }
  categories: Category[]
  changeImage: boolean = false
  fileUpload: any
  imageCompress: any
  currentImage: string

  private movieId: string
  constructor(
    private activatedRoute: ActivatedRoute,
    private categoryService: CategoriesService ,
    private moviesService: MoviesService ,
    private router:Router,
    private storage: AngularFireStorage,
    private imgCompressService: ImageCompressService 

  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => {
        this.movieId = params.get('movieId')  
        if(this.movieId){
          this.loadInfo(this.movieId);
          
        }  
      }, 
      ( error ) => {

      } 
     )
    this.loadCategories()
    
   /*
    source.subscribe( (data => { 
      console.log(typeof data)
      console.log(data)
      if(  typeof data.count === 'undefined' ){
        this.cs.create( this.chatId, this.type_item )
      }else{
        this.chat$ = this.cs.joinUsers(source);
      }
    }));*/
  }

  loadCategories(){
    this.categoryService.list().subscribe(
      (detail:any) => { 
        console.log(detail)
         this.categories = detail
         //console.log(this.categories)
      },
      (error) => { 
        console.log(error) 
      }
    );
  }

  onFileChange(event){ 
    this.fileUpload = event.target.files[0];
 
    let files = event.target.files;

    // ImageCompressService.filesToCompressedImageSource(files).then(observableImages => {
    //   observableImages.subscribe((image) => { 
    //      this.dataURLtoBlob( image.compressedImage.imageDataUrl, function( blob )
    //       {
    //         this.imageCompress = blob;
    //       });
    //   }, (error) => {
    //     console.log("Error while converting");
    //   }, () => {
                         
    //   });
    // });
    
    
  }

  dataURLtoBlob( dataUrl, callback )
  {
      var req = new XMLHttpRequest;

      req.open( 'GET', dataUrl );
      req.responseType = 'arraybuffer'; // Can't use blob directly because of https://crbug.com/412752

      req.onload = function fileLoaded(e)
      {
          // If you require the blob to have correct mime type
          var mime = this.getResponseHeader('content-type');

          callback( new Blob([this.response], {type:mime}) );
      }; 
      req.send();
  }

  
 

  actionStore(){  
    if( this.fileUpload !== undefined ){
      const file = this.fileUpload;
      const filePath = file.name;
      const task2 = this.storage.upload( environment.firebase.folderstore + filePath, file ); 
      this.movie.image = this.fileUpload.name;
    } 

    let metodo = (this.movieId) ? this.moviesService.update(this.movieId, this.movie) : this.moviesService.store(this.movie);
    metodo.subscribe(
      () => { 
         this.router.navigate(['/movies'])
      },
      (error) => { 
        console.log(error) 
      }
    );
  } 

  loadInfo(movieId:string){
    this.moviesService.get(movieId).subscribe(
      (detail:any) => {  
         this.movie = detail
         this.currentImage = `${environment.firebase.authDomainStorage}${environment.firebase.storageBucket}/o/avatar%2F${this.movie.image}?alt=media`;
      },
      (error) => { 
        console.log(error) 
      }
    );
  }

  

}
