import { Component, OnInit, ViewChild } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { DialogremoveMoviesComponent } from '../../movies/dialogremove-movies/dialogremove-movies.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Movie } from '../../../interface/movie'
import { Category } from '../../../interface/category'
import { ReadfirebasePipe } from '../../../pipes/readfirebase.pipe'
import { environment } from 'src/environments/environment.prod';
import { SnackbarComponent } from '../../shared/snackbar/snackbar.component';
import { LightboxComponent } from '../../shared/lightbox/lightbox.component';
import { CardComponent } from '../../shared/card/card.component';
import { TablelistComponent } from '../../shared/tablelist/tablelist.component';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css'],
  providers: [ ReadfirebasePipe ]
})
export class ListMoviesComponent implements OnInit {
  listMovies: Movie[] = []
  listCategories: Category[] = []
  isLoadingResults:boolean = true
  viewStyle = "card"
  albumLB = []
  public colorMode: string = 'white'
  @ViewChild('thecard') thecard: CardComponent
  @ViewChild('thelist') thelist: TablelistComponent

  constructor(
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private moviesService: MoviesService,
    private categoriesService: CategoriesService,
    private fbPipe: ReadfirebasePipe 
  ) { }

  ngOnInit() {
    this.loadMovies() 
  }  

  changeView(e:any) {
    this.viewStyle = e.value;
  } 

  loadMovies() { 
    this.isLoadingResults = true;
    this.categoriesService.list().subscribe(
      (list: any) => {
        this.listCategories = this.fbPipe.transform(list);
        this.moviesService.list().subscribe(
          (listM: any) => {
            this.listMovies = this.fbPipe.transform(listM);  
            for( let key in this.listMovies ){ 
              let result = this.listCategories.find(category => category.id == this.listMovies[key].categories_id);
              this.listMovies[key].categories_name = result.name
              this.listMovies[key].image = `${environment.firebase.authDomainStorage}${environment.firebase.storageBucket}/o/avatar%2F${this.listMovies[key].image}?alt=media`;
            }
            this.isLoadingResults = false;
            this.loadLB()
          }
        );
      }
    ); 
  }

  loadLB(){
    this.listMovies.forEach(movie => {
      const src = movie.image;
      const caption = movie.name;
      const thumb = movie.image;
      const id = movie.id;
      const album = {
         id: id,
         src: src,
         caption: caption,
         thumb: thumb
      };

      this.albumLB.push(album);
    }); 
  }

  openLB(m:Movie){ 
    let indexLB = this.listMovies.findIndex(movie => movie.id === m.id);

    const lb = this.dialog.open(LightboxComponent, {
      width: 'initial',
      data: {album: this.albumLB, index: indexLB}
    }); 
  } 

  addCategoria(m:Movie) {
    this.moviesService.store(m).subscribe(
      () => {
        this.loadMovies();
      }
    )
  }

  openDialogRemove(m:Movie): void {
    const dialogRef = this.dialog.open(DialogremoveMoviesComponent, {
      width: '400px',
      data: {movie: m}
    });

    dialogRef.componentInstance.actionOk.subscribe((m:Movie) => { 
      this.actionRemove(m) 
      dialogRef.close();
    });
  }

  actionRemove(m:Movie): void {
     this.moviesService.delete(m.id).subscribe(
       (r) => {
         if(r.status == 200){
          this.showSB("Pelicula eliminada correctamente", "success")
          this.loadMovies()
         }
         else{
          this.showSB("Error eliminando pelicula", "error")
         }
       }
     )
  }

  changeColorMode(e:any) { 
    this.colorMode = e.value  
  }

  showSB(message: string, type:string, duration:number = 3000) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: duration,
      data: { message: message, type: type }
    });
  }

}
