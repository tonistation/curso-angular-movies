import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogremoveMoviesComponent } from './dialogremove-movies.component';

describe('DialogremoveMoviesComponent', () => {
  let component: DialogremoveMoviesComponent;
  let fixture: ComponentFixture<DialogremoveMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogremoveMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogremoveMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
