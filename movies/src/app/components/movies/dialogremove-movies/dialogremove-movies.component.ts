import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'; 
import { Movie } from 'src/app/interface/movie';


@Component({
  selector: 'app-dialogremove-movies',
  templateUrl: './dialogremove-movies.component.html',
  styleUrls: ['./dialogremove-movies.component.css']
})
export class DialogremoveMoviesComponent  {
  actionOk = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogremoveMoviesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onButtonClick(m:Movie) {
    this.actionOk.emit(m);
  } 

}

export interface DialogData { 
  movie: any;
}