import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { Movie } from 'src/app/interface/movie';
import { CategoriesService } from 'src/app/services/categories.service';
import { Category } from 'src/app/interface/category';
import { ReadfirebasePipe } from '../../../pipes/readfirebase.pipe'
import { environment } from 'src/environments/environment.prod'; 

@Component({
  selector: 'app-view-movies',
  templateUrl: './view-movies.component.html',
  styleUrls: ['./view-movies.component.css'],
  providers: [ ReadfirebasePipe ]
})
export class ViewMoviesComponent implements OnInit {

  movieId: string
  movie: Movie = {
    name: '',
    description: '',
  }
  imdbData: {}
  categories: Category[]
  isLoadingResults: boolean = true

  constructor(
    private activatedRoute: ActivatedRoute,
    private moviesService: MoviesService , 
    private router:Router,
    private categoryService: CategoriesService,
    private fbPipe: ReadfirebasePipe
  ) { }
 
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => {
        this.movieId = params.get('movieId')  
        if(this.movieId){
         this.loadInfo(this.movieId)         
        }  
      }, 
      ( error ) => {
        
      } 
     )
      
  }

  loadInfo(movieId:string){
    this.isLoadingResults = true;
    this.moviesService.get(movieId).subscribe(
      (detail:any) => {  
         this.movie = detail
         this.isLoadingResults = false;
         this.movie.image = `${environment.firebase.authDomainStorage}${environment.firebase.storageBucket}/o/avatar%2F${this.movie.image}?alt=media`;
         this.loadCategories()
      },
      (error) => { 
        console.log(error) 
      }
    );
  }

  loadCategories(){
    this.categoryService.list().subscribe(
      (detail:any) => { 
         this.categories = this.fbPipe.transform(detail); 
         let result = this.categories.find(category => category.id == this.movie.categories_id);
         this.movie.categories_name = result.name 

         if(this.movie.idimdb){
          this.moviesService.getImdb(this.movie.idimdb).subscribe( response => {
            this.imdbData = response;
          } );
         } 
      },
      (error) => { 
        console.log(error) 
      }
    );
  }


}
