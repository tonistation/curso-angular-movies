import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAdminFeedComponent } from './form-admin-feed.component';

describe('FormAdminFeedComponent', () => {
  let component: FormAdminFeedComponent;
  let fixture: ComponentFixture<FormAdminFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAdminFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAdminFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
