import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedsService } from 'src/app/services/feeds.service';
import { UtilsService } from 'src/app/services/utils.service'
import { Feed } from 'src/app/interface/feed';

@Component({
  selector: 'app-form-admin-feed',
  templateUrl: './form-admin-feed.component.html',
  styleUrls: ['./form-admin-feed.component.css']
})
export class FormAdminFeedComponent implements OnInit {

  feed: Feed = {
    name: '',
    description: '',
    url: ''
  }
  isLoadingResults:boolean = true 
  feedId: any
  constructor(
    private activatedRoute: ActivatedRoute,
    private feedService: FeedsService,
    private router:Router,
    private utilService: UtilsService
  ) { }

  ngOnInit() {
    this.isLoadingResults = true
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => { 
        this.feedId = params.get('feedId')  
        if(this.feedId){
          this.loadInfo(this.feedId)
        }else{
          this.isLoadingResults = false
        }  
      }, 
      ( error ) => {
        
      } 
     )
  }

  loadInfo(feedId: any) { 
    this.feedService.getInfo(feedId).subscribe(
      (data: Feed) => {
        this.isLoadingResults = false
        this.feed = data
      }
    )
  }

  actionStore(){    
    let metodo = (this.feedId) ? this.feedService.update(this.feedId, this.feed) : this.feedService.store(this.feed);
    metodo.subscribe(
      () => { 
          this.utilService.showSB('Feed Guardado', 'success') 
          this.router.navigate(['/feeds/admin'])
      },
      (error) => { 
        console.log(error) 
      }
    );
  }  
}
