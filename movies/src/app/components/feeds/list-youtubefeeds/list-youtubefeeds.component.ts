import { Component, OnInit, ViewChild } from '@angular/core'; 
import { UtilsService } from 'src/app/services/utils.service'
import { MatDialog } from '@angular/material';
import { DialogvideoComponent } from '../../feeds/dialogvideo/dialogvideo.component'; 
import * as _ from 'lodash' 
import { YoutubefeedsService } from 'src/app/services/youtubefeeds.service';

@Component({
  selector: 'app-list-youtubefeeds',
  templateUrl: './list-youtubefeeds.component.html',
  styleUrls: ['./list-youtubefeeds.component.css']
})
export class ListYoutubefeedsComponent implements OnInit {
  listFeeds: any[] = []
  isLoadingResults:boolean = true 
  feedSelect: any
  page: number = 1;

  constructor(
    private youtubeFeedService: YoutubefeedsService,
    public dialog: MatDialog,
    private utilService: UtilsService
  ) { }

  ngOnInit() {
    this.loadFeeds() 
  }  
 

  loadFeed(f) {
    const indexFeed = this.listFeeds.findIndex(feed => feed.id === f.id)
    this.feedSelect = this.listFeeds[indexFeed].feed
  }

  loadFeeds() { 
    this.isLoadingResults = true;
    this.youtubeFeedService.list(
      (list) => { 
        this.listFeeds = list; 
        this.isLoadingResults = false;   
        this.feedSelect =  this.listFeeds['0'].feed
        console.log(this.feedSelect)
      }
    ) 
  }

  openVideo(f){ 
    console.log(f) 

    const lb = this.dialog.open(DialogvideoComponent, {
      width: 'initial',
      data: f
    });
  }  

}
