import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListYoutubefeedsComponent } from './list-youtubefeeds.component';

describe('ListYoutubefeedsComponent', () => {
  let component: ListYoutubefeedsComponent;
  let fixture: ComponentFixture<ListYoutubefeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListYoutubefeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListYoutubefeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
