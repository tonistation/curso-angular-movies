import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogremoveFeedsComponent } from './dialogremove-feeds.component';

describe('DialogremoveFeedsComponent', () => {
  let component: DialogremoveFeedsComponent;
  let fixture: ComponentFixture<DialogremoveFeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogremoveFeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogremoveFeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
