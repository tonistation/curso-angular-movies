import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'; 
import { Movie } from 'src/app/interface/movie';


@Component({
  selector: 'app-dialogremove-feeds',
  templateUrl: './dialogremove-feeds.component.html',
  styleUrls: ['./dialogremove-feeds.component.css']
})
export class DialogremoveFeedsComponent {

  actionOk = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogremoveFeedsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onButtonClick(m:Movie) {
    this.actionOk.emit(m);
  } 
}

export interface DialogData { 
  feed: any;
}