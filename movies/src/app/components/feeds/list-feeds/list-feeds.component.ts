import { Component, OnInit, ViewChild } from '@angular/core';
import { FeedsService } from 'src/app/services/feeds.service'; 
import { UtilsService } from 'src/app/services/utils.service'
import { MatDialog } from '@angular/material';
import { LightboxComponent } from '../../shared/lightbox/lightbox.component'; 
import * as _ from 'lodash' 


@Component({
  selector: 'app-list-feeds',
  templateUrl: './list-feeds.component.html',
  styleUrls: ['./list-feeds.component.css'] 
})

export class ListFeedsComponent implements OnInit {
  listFeeds: any[] = []
  isLoadingResults:boolean = true 
  feedSelect: any
  page: number = 1;
 
  constructor(
    private feedService: FeedsService,
    public dialog: MatDialog,
    private utilService: UtilsService
  ) { }

  ngOnInit() {
    this.loadFeeds() 
  }  
 

  loadFeed(f) {
    const indexFeed = this.listFeeds.findIndex(feed => feed.id === f.id)
    this.feedSelect = { ...this.listFeeds[indexFeed].feed.rss.channel, name: this.listFeeds[indexFeed].name } 
    //console.log(this.feedSelect)
  }

  loadFeeds() { 
    this.isLoadingResults = true;
    this.feedService.list(
      (list) => {
        this.listFeeds = list; 
        this.isLoadingResults = false;   
          this.feedSelect =  { ...this.listFeeds['0'].feed.rss.channel, name: this.listFeeds['0'].name } 
      }
    ) 
  }

  openLB(f){ 
    //console.log(f)
    let indexLB = this.feedSelect.item.findIndex(feed => feed.id === f.id);
    const album = {
      id: f.id,
      src: f.image,
      caption: f.title, 
      thumb: f.image
    };

    let albumLB = [ album ]

    const lb = this.dialog.open(LightboxComponent, {
      width: 'initial',
      data: {album:albumLB, index: indexLB}
    }); 
  }  

}
