import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'app-dialogvideo',
  templateUrl: './dialogvideo.component.html',
  styleUrls: ['./dialogvideo.component.css']
})
export class DialogvideoComponent  {
  yt_iframe_html: any;
  constructor(
    public dialogRef: MatDialogRef<DialogvideoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private embedService: EmbedVideoService
  ) { 
    this.yt_iframe_html = this.embedService.embed(this.data.link);
  }
 

}

export interface DialogData { 
  link: string,
  author: {any},
  text: [],
  'media:group': {any},
  description: string,
  id: string,
  image: string,
  published: string,
  updated: string,
  title: string,
  videoid: string,
  'yt:channelId' : string,
  'yt:videoId' : string
}
