import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogvideoComponent } from './dialogvideo.component';

describe('DialogvideoComponent', () => {
  let component: DialogvideoComponent;
  let fixture: ComponentFixture<DialogvideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogvideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogvideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
