import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMoviesFeedsComponent } from './list-movies-feeds.component';

describe('ListMoviesFeedsComponent', () => {
  let component: ListMoviesFeedsComponent;
  let fixture: ComponentFixture<ListMoviesFeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMoviesFeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMoviesFeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
