import { Component, OnInit } from '@angular/core';
import { FeedsService } from 'src/app/services/feeds.service';
import { ReadfirebasePipe } from '../../../pipes/readfirebase.pipe'
import { DialogremoveFeedsComponent } from '../dialogremove-feeds/dialogremove-feeds.component';
import { Feed } from 'src/app/interface/feed';
import { MatDialog } from '@angular/material';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-admin-feeds',
  templateUrl: './admin-feeds.component.html',
  styleUrls: ['./admin-feeds.component.css'],
  providers: [ ReadfirebasePipe ]
})
export class AdminFeedsComponent implements OnInit {

  listFeeds: any
  isLoadingResults:boolean = true 
  viewStyle = "card"
  constructor(
    private feedService: FeedsService,
    private fbPipe: ReadfirebasePipe,
    private dialog: MatDialog,
    private utilService: UtilsService
  ) { }

  ngOnInit() { 
    this.loadFeedsList()
  }

  loadFeedsList() {
    this.isLoadingResults = true
    this.feedService.listAdmin().subscribe(
      (list:[]) => {
        this.listFeeds = this.fbPipe.transform(list);
        this.isLoadingResults = false
      }
    )
  }

  changeView(e:any) {
    this.viewStyle = e.value;
  } 

  openDialogRemove(f:Feed): void {
    const dialogRef = this.dialog.open(DialogremoveFeedsComponent, {
      width: '400px',
      data: {feed: f}
    });

    dialogRef.componentInstance.actionOk.subscribe((f:Feed) => { 
      this.actionRemove(f) 
      dialogRef.close();
    });
  }

  actionRemove(f:Feed): void {
    this.feedService.delete(f.id).subscribe(
      (r) => {
        if(r.status == 200){
         this.utilService.showSB("Fuente RSS eliminada correctamente", "success")
         this.loadFeedsList()
        }
        else{
         this.utilService.showSB("Error eliminando Fuente RSS", "error")
        }
      }
    )
 }

}
