import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedsService } from 'src/app/services/feeds.service';
import { DialogremoveFeedsComponent } from '../dialogremove-feeds/dialogremove-feeds.component';
import { Feed } from 'src/app/interface/feed';
import { MatDialog } from '@angular/material';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-view-admin-feed',
  templateUrl: './view-admin-feed.component.html',
  styleUrls: ['./view-admin-feed.component.css']

})
export class ViewAdminFeedComponent implements OnInit {
 
  feed: any
  isLoadingResults:boolean = true 
  public feedId: any
  constructor(
    private activatedRoute: ActivatedRoute,
    private feedService: FeedsService,
    private dialog: MatDialog,
    private utilService: UtilsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.isLoadingResults = true
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => { 
        this.feedId = params.get('feedId')  
        if(this.feedId){
          this.loadInfo(this.feedId)
        }  
      }, 
      ( error ) => {
        
      } 
     )
  }

  loadInfo(feedId: any) { 
    this.feedService.getInfo(feedId).subscribe(
      (data: any) => {
        this.isLoadingResults = false
        this.feed = data
      }
    )
  }

  openDialogRemove(): void {
    let f = this.feed
    const dialogRef = this.dialog.open(DialogremoveFeedsComponent, {
      width: '400px',
      data: {feed: f}
    });

    dialogRef.componentInstance.actionOk.subscribe((f:Feed) => { 
      this.actionRemove(f) 
      dialogRef.close();
    });
  }

  actionRemove(f:Feed): void {
    this.feedService.delete(f.id).subscribe(
      (r) => {
        if(r.status == 200){
         this.utilService.showSB("Fuente RSS eliminada correctamente", "success")
         this.router.navigate(['/feeds/admin'])
        }
        else{
         this.utilService.showSB("Error eliminando Fuente RSS", "error")
        }
      }
    )
  }
}
