import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAdminFeedComponent } from './view-admin-feed.component';

describe('ViewAdminFeedComponent', () => {
  let component: ViewAdminFeedComponent;
  let fixture: ComponentFixture<ViewAdminFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAdminFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAdminFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
