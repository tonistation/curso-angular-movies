import { Component, OnInit } from '@angular/core';
import { } from 'googlemaps'
import { SocketiomapService } from 'src/app/services/socketiomap.service';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  map: google.maps.Map
  marker: google.maps.Marker
  constructor(
    private socketIo: SocketiomapService
  ) { }

  ngOnInit() {
    this.loadMapa( -12.0532268, -77.0383178) 
    this.socketIo.on('nuevaPosicion', (data) => {
      console.log(data)
    })
  }

  loadMapa ( latitud, longitud ){
    const mapProp = {
      center: new google.maps.LatLng(latitud, longitud),
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
    }
    this.map = new google.maps.Map(document.getElementById('gmap'), mapProp)
    google.maps.event.addListener(this.map, 'click', (event) => {
      this.googleAddMaker(event.latLng)
      console.log(event.latLng)
    })
  }

  googleAddMaker ( location ){
    if (this.marker ) {
      this.marker.setMap(null)
    }
    this.marker = new google.maps.Marker({
      position: location,
      map: this.map
    })

    this.marker.setMap(this.map)
    this.map.setCenter(location)
  }

}
