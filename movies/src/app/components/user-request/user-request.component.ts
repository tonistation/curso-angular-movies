import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/interface/user';

@Component({
  selector: 'app-user-request',
  templateUrl: './user-request.component.html',
  styleUrls: ['./user-request.component.css']
})
export class UserRequestComponent implements OnInit {

  listRequest: any  
  isLoadingResults:boolean = true 
  public userId: string

  constructor( 
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.loadInfo()
  }

  loadInfo() { 
    this.isLoadingResults = true
    this.userService.getUsersRequest().subscribe(
      (result: any) => {
        this.isLoadingResults = false 
        this.listRequest = result 
      }
    )
  }

}
 