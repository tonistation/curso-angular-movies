import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEvaluateRequestComponent } from './dialog-evaluate-request.component';

describe('DialogEvaluateRequestComponent', () => {
  let component: DialogEvaluateRequestComponent;
  let fixture: ComponentFixture<DialogEvaluateRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEvaluateRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEvaluateRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
