import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material' 
import { UserRequest } from 'src/app/interface/user-request';  
@Component({
  selector: 'app-dialog-evaluate-request',
  templateUrl: './dialog-evaluate-request.component.html',
  styleUrls: ['./dialog-evaluate-request.component.css']
})
export class DialogEvaluateRequestComponent implements OnInit {
  grantAccessOk = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogEvaluateRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserRequest[]
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
    console.log(this.data);
  }

  grantAccess(ur, index){   
    this.grantAccessOk.emit(ur.id);  
    if( this.data.length == 1 ){ 
      this.onNoClick()
    }
    
  }

}
 
 