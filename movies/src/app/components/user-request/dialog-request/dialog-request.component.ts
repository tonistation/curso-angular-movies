import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material' 
import { UserRequest } from 'src/app/interface/user-request';

@Component({
  selector: 'app-dialog-request',
  templateUrl: './dialog-request.component.html',
  styleUrls: ['./dialog-request.component.css']
})
export class DialogRequestComponent implements OnInit {
  actionOk = new EventEmitter();
  
  constructor(
    public dialogRef: MatDialogRef<DialogRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserRequest
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
  }

  requestAccess(){ 
    this.actionOk.emit(this.data); 
    this.onNoClick();
  }

} 
 