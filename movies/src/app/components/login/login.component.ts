import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service'; 
import { UtilsService } from 'src/app/services/utils.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms'; 
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Router } from  "@angular/router";  

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})
export class LoginComponent implements OnInit, AfterViewInit {
  username: ''
  password: '' 
  myGroup: any
  formArray: any
  private targetInput = 'input0';

  constructor(
    private authsevice: AuthService, 
    private utils: UtilsService,
    private _formBuilder: FormBuilder,
    public  router:  Router
  ) { }

  ngOnInit() 
  {
    if(this.authsevice.isLoggedIn)
      this.router.navigate(['/']);  

    this.myGroup = this._formBuilder.group({
      formArray: this._formBuilder.array([
        this._formBuilder.group({
          username: ['',Validators.required]
        }),
        this._formBuilder.group({
          password: new FormControl()
        })
      ])
    });
  }

  ngAfterViewInit() {
    this.setFocus();
  }

  private setFocus() {
    let targetElem = document.getElementById(this.targetInput);
    setTimeout(function waitTargetElem() {
      if (document.body.contains(targetElem)) {
        targetElem.focus();
      } else {
        setTimeout(waitTargetElem, 100);
      }
    }, 100);
  }

  onChange(event: any) {
    let index = String(event.selectedIndex);
    this.targetInput = 'input' + index;
    this.setFocus();
  }
   

  loginGoogle(){
    this.authsevice.loginGoogle()
  }

  loginFacebook(){
    this.authsevice.loginFacebook()
  }

  loginTwitter(){
    this.authsevice.loginTwitter()
  }

  login( ){  
    const credentials = {
      username: this.myGroup.value.formArray[0].username,
      password: this.myGroup.value.formArray[1].password
    }  
    this.authsevice.login( credentials )
    .then(
       (result) => {  
        /*this.utils.showSB('Bienvenido', 'success')
        this.router.navigate(['/']);  */
        this.authsevice.loginNormalSuccess(result);
       }
    )
    .catch(
      (error) =>{  
        if(error.code == "auth/invalid-email"){
          this.utils.showSB('Error, correo invalido', 'error')
        }
        if(error.code == "auth/wrong-password"){
          this.utils.showSB('Error, Usuario o contraseña invalida', 'error')
        }
      }
    )
  }

}
