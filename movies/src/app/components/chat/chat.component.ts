import { Component, OnInit } from '@angular/core';
import { SocketioService } from 'src/app/services/socketio.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  listMessages: any[] = []
  message: string
  constructor(
    private socketio: SocketioService
  ) { }

  ngOnInit() {
    this.socketio.on('newMessage', (data) => {
      this.listMessages.push(data)
    })
  }

  addMessage() {
    this.socketio.emit('newMessage', { 'message': this.message, socket: '' })
    this.message = ''
  }

}
