import { Component, OnInit, ViewChild } from '@angular/core';
import { CommentFirestoreService } from 'src/app/services/comment-firestore.service';
import { CardComponent } from '../../shared/card/card.component';
import { TablelistComponent } from '../../shared/tablelist/tablelist.component';
import Chart from 'chart.js';
import { SnackbarComponent } from '../../shared/snackbar/snackbar.component';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-list-comments',
  templateUrl: './list-comments.component.html',
  styleUrls: ['./list-comments.component.css']
})
export class ListCommentsComponent implements OnInit {
  listMessages = []
  isLoadingResults:boolean = true
  viewStyle = "card"
  message: string
  colorMode: string = 'white'
  @ViewChild('thecard') thecard: CardComponent
  @ViewChild('thelist') thelist: TablelistComponent

  constructor(
    private cfs: CommentFirestoreService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.loadComments()
    this.loadGraph()
  } 

  changeView(e:any) {
    this.viewStyle = e.value;
  }


  loadComments(){
    this.isLoadingResults = true;
    this.cfs.load().subscribe(
      (result:any)=>{
        this.listMessages = result 
        this.isLoadingResults = false;
      }
    )
  }

  addComment( texto:string ){
    this.cfs.addComment(texto)
    this.showSB("Añadido","success")
  }

  changeColorMode(e:any) { 
      this.colorMode = e.value 
  }

  showSB(message: string, type:string, duration:number = 3000) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: duration,
      data: { message: message, type: type }
    });
  }

  loadGraph(){
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
  }
}
