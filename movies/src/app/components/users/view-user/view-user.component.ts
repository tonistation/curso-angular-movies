import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/interface/user';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  user: User  
  isLoadingResults:boolean = true 
  public userId: string

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.isLoadingResults = true
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => { 
        this.userId = params.get('userId')  
        if(this.userId){
          this.loadInfo(this.userId)
        }  
      }, 
      ( error ) => {
        
      } 
     )
  }

  loadInfo(userId: string) { 
    this.isLoadingResults = true
    this.userService.getUser(userId).subscribe(
      (result: any) => {
        this.isLoadingResults = false 
         this.user = result
         console.log(this.user)
      }
    )
  }

  openDialogAccess(){}

}
