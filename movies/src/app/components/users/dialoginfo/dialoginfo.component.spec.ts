import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialoginfoComponent } from './dialoginfo.component';

describe('DialoginfoComponent', () => {
  let component: DialoginfoComponent;
  let fixture: ComponentFixture<DialoginfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialoginfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialoginfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
