import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialoginfo',
  templateUrl: './dialoginfo.component.html',
  styleUrls: ['./dialoginfo.component.css']
}) 

export class DialoginfoComponent {

  constructor(
    public dialogRef: MatDialogRef<DialoginfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit() {
  }

}

export interface DialogData { 
  name: string;
}