import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../../interface/user';

@Component({
  selector: 'app-dialogremove',
  templateUrl: './dialogremove.component.html',
  styleUrls: ['./dialogremove.component.css']
})
export class DialogremoveComponent{
  onAdd = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogremoveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  } 

  onButtonClick() {
    this.onAdd.emit();
  }

  remove(u:User) {
    this.onAdd.emit(u);
  }

  ngOnInit() {
  } 

}

export interface DialogData { 
  user: any;
}
