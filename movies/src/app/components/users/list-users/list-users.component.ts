import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';  
import { DialogremoveComponent } from '../../users/dialogremove/dialogremove.component';
import { MatDialog } from '@angular/material';
import { User } from '../../../interface/user'
import { UserService } from '../../../service/user.service'
import { LightboxComponent } from '../../shared/lightbox/lightbox.component';
import * as _ from 'lodash'

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})

export class ListUsersComponent implements OnInit {
    
  users: User[]; 
  isLoadingResults:boolean = true 
  viewStyle = "card"
  albumLB = []

  dataSource: MatTableDataSource<User>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private userService: UserService,
    public dialog: MatDialog
  ) { } 

  ngOnInit() { 
    this.loadUsers();
  }


/*
  getListUsers = (page:number, per_page:number = 10) => {
    this.isLoadingResults = true;
    this.userService.getListUser(page, per_page).then(
      (result: any) => { 
        this.users = result.data.map((user)=>({ 
          firstName: user.first_name,
          lastName: user.last_name,
          ...user
        }))
        //console.log(this.users)
        
        this.isLoadingResults = false;      
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (error) => { 
        console.log(error)
        this.isLoadingResults = false;
      }
    );
  }
  */
  changeView(e) {
   this.viewStyle = e.value;
  }


 loadUsers = () => {
    this.isLoadingResults = true;
    this.userService.list().subscribe(
      (result: User[]) => {  
        for( let u of result ){
          u.name = u.displayName
          u.image = u.photoURL
          u.id = u.uid
        }
        this.users = result;  
        this.loadLB()
        this.isLoadingResults = false;      
      },
      (error) => { 
        console.log(error)
        this.isLoadingResults = false;
      }
    );
  }

  loadLB(){
    this.users.forEach(user => {
      const src = user.photoURL;
      const caption = user.displayName;
      const thumb = user.photoURL;
      const id = user.uid;
      const album = {
         id: id,
         src: src,
         caption: caption,
         thumb: thumb
      };

      this.albumLB.push(album);
    }); 
  }
 

  openLB(u:User){ 
    let indexLB = this.users.findIndex(user => user.id === u.id);  
    const lb = this.dialog.open(LightboxComponent, {
      width: 'initial',
      data: {album: this.albumLB, index: indexLB}
    }); 
  } 

  openDialogPreview(u):void {
    this.userService.openDialogPreview(u.firstName)
  } 

  remove(u):void {
    console.log(u, "remove")
  }
 
  
  openDialogRemove(user:User): void {
    const dialogRef = this.dialog.open(DialogremoveComponent, {
      width: '400px',
      data: {user: user}
    });

    dialogRef.componentInstance.onAdd.subscribe((u:User) => { 
      this.actionRemove(u) 
      dialogRef.close();
    });
  }

  actionRemove(u:User): void {
    let indexUserRemove = this.users.findIndex( user => user.id == u.id  )
    //let userRemove = this.users.map( user => user._id == u._id  ) 
    let dataU = this.users;
    dataU.splice( indexUserRemove, 1 )    
    this.dataSource = new MatTableDataSource<User>(dataU); 
    this.dataSource.sort = this.sort;
  }

  checkAdminRequest(){
    this.userService.checkUserRequest();
  }
}
