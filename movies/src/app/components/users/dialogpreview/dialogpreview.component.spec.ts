import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogpreviewComponent } from './dialogpreview.component';

describe('DialogpreviewComponent', () => {
  let component: DialogpreviewComponent;
  let fixture: ComponentFixture<DialogpreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogpreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogpreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
