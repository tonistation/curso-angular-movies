import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogpreview',
  templateUrl: './dialogpreview.component.html',
  styleUrls: ['./dialogpreview.component.css']
})
export class DialogpreviewComponent{

  constructor(
    public dialogRef: MatDialogRef<DialogpreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}

export interface DialogData { 
  name: string;
}
