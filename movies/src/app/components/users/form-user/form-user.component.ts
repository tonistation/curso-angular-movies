import { Component, OnInit } from '@angular/core';
import  { ActivatedRoute, Router } from '@angular/router'
import { User } from '../../../interface/user'
import { UserService } from '../../../service/user.service'
import * as _ from 'lodash'

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.css']
})
export class FormUserComponent implements OnInit {

  user: User = { 
    photoURL: '',
    displayName: '',
    email: '',  
    bgmodel:'',
    theme:''
  }

  user_levels: any
  isLoadingResults:boolean = true 
  public userId: string
 
  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService ,
    private router:Router
  ) { }

  ngOnInit() { 
    this.isLoadingResults = true
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => { 
        this.userId = params.get('userId')  
        if(this.userId){
          this.loadInfo(this.userId)
        }  
      }, 
      ( error ) => {
        
      } 
     )
  }

  loadInfo(userId: string) { 
    this.isLoadingResults = true
    this.userService.getUser(userId).subscribe(
      (result: any) => {
        this.isLoadingResults = false 
        this.user = result 
        this.loadLevels()
      }
    )
  }
 
  loadLevels() {  
    this.isLoadingResults = true
    this.userService.listLevels().subscribe(
      (result: any) => {
        this.isLoadingResults = false  
        this.user_levels = result 

      }
    )
  }

  actionStore(){   
    console.log(this.user)
    let newLevel:number = this.user.level
    let metodo = (this.userId) ? this.userService.update(this.userId, { level: newLevel} ) : this.userService.store(this.user);
    metodo.subscribe(
      (result) => {
         console.log(result) 
         this.router.navigate(['/users'])
      },
      (error) => { 
        console.log(error) 
      }
    );
  } 



}
