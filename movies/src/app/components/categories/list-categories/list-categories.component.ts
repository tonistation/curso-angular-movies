import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriesService } from 'src/app/services/categories.service'; 
import { MatDialog, MatSnackBar } from '@angular/material';
import { Category } from '../../../interface/category'
import { SnackbarComponent } from '../../shared/snackbar/snackbar.component';
import { DialogremoveCategoryComponent } from '../dialogremove-category/dialogremove-category.component';
import { ReadfirebasePipe } from 'src/app/pipes/readfirebase.pipe';
import { Movie } from 'src/app/interface/movie';
import { MoviesService } from 'src/app/services/movies.service'; 
import { CardComponent } from '../../shared/card/card.component';
import { TablelistComponent } from '../../shared/tablelist/tablelist.component';
import * as _ from 'lodash'

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.css'],
  providers: [ ReadfirebasePipe ]
})
export class ListCategoriesComponent implements OnInit {
  listCategories: any[] = []
  listMovies: Movie[] = []
  isLoadingResults:boolean = true
  viewStyle = "card" 
  colorMode: string = 'white'
  @ViewChild('thecard') thecard: CardComponent
  @ViewChild('thelist') thelist: TablelistComponent

  constructor(
    public dialog: MatDialog, 
    private catService: CategoriesService, 
    private moviesService: MoviesService,
    private snackBar: MatSnackBar,
    private fbPipe: ReadfirebasePipe
  ) { }

  ngOnInit() {
    this.loadCategorias()
  } 

  changeView(e) {
    this.viewStyle = e.value;
  }

  loadCategorias() {
    this.isLoadingResults = true;
    this.catService.list().subscribe(
      (list: any) => { 
        this.listCategories = this.fbPipe.transform(list);
        this.moviesService.list().subscribe(
          (listM: any) => {
            this.listMovies = this.fbPipe.transform(listM);  
            let resultGroupMovies = _.chain(this.listMovies)
                                    .groupBy("categories_id")
                                    .toPairs()
                                    .map(function(currentData){
                                      return _.zipObject(["categorie_id", "movies"], currentData);
                                    })
                                    .value();
            
            for( let key in this.listCategories ){
              let result = resultGroupMovies.find(movieGroup => movieGroup.categorie_id == this.listCategories[key].id );
              this.listCategories[key].total_movies = ( result !== undefined ) ? result.movies.length : 0;
              this.listCategories[key].movies = ( result !== undefined ) ? _.cloneDeep(result.movies) : [];
            } 
            this.isLoadingResults = false; 
          }
        );
      }
    );
  }
 

  addCategoria(c:Category) {
    this.catService.store(c).subscribe(
      () => {
        this.loadCategorias();
      }
    )
  }

  openDialogRemove(c:Category): void {
    const dialogRef = this.dialog.open(DialogremoveCategoryComponent, {
      width: '400px',
      data: {category: c}
    });

    dialogRef.componentInstance.actionOk.subscribe((c:Category) => { 
      this.actionRemove(c, false) 
      dialogRef.close();
    });

    dialogRef.componentInstance.actionOkAll.subscribe((c:Category) => { 
      this.actionRemove(c, true) 
      dialogRef.close();
    });
  }

  actionRemove(c:Category, deleteAll: boolean): void {
    
    this.catService.delete(c.id).subscribe(
      (r) => {
        if(r.status == 200){
          this.showSB("Categoria eliminada correctamente", "success")
          this.loadCategorias()
 
          this.showSB("Procesando peliculas vinculadas", "info")
          let cont = 0;
          for( let key in c.movies){
            let metodo = (deleteAll) ? this.moviesService.delete(c.movies[key].id) : this.moviesService.update(c.movies[key].id, { categories_id: 'general' })
            metodo.subscribe(
              (r) => {
                if(r.status == 200){
                  cont++;
                }
              }
            )
          }
          if(c.movies.length == cont){
            let msg = (deleteAll) ? "eliminadas" : "actualizadas"
            this.showSB("Peliculas " + msg + " correctamente", "success")
          }else{
            let msg = (deleteAll) ? "eliminando" : "actualizando"
            this.showSB("Error "+ msg +" peliculas", "error")
          } 
        }
        else{
          this.showSB("Error eliminando pelicula", "error")
        }
      }
    ) 
  } 

  changeColorMode(e:any) { 
      this.colorMode = e.value; 
  }

  showSB(message: string, type:string, duration:number = 3000) {
    this.snackBar.openFromComponent(SnackbarComponent, {
      duration: duration,
      data: { message: message, type: type }
    });
  }

}
