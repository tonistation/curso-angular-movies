import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Category } from '../../../interface/category'
import { CategoriesService } from '../../../services/categories.service'

@Component({
  selector: 'app-form-categories',
  templateUrl: './form-categories.component.html',
  styleUrls: ['./form-categories.component.css']
})
export class FormCategoriesComponent implements OnInit {
  isLoadingResults: boolean = false
  category: Category = {
    name: '',
    description: ''
  }

  private categoryId: string

  constructor(
    private activatedRoute: ActivatedRoute,
    private categoryService: CategoriesService ,
    private router:Router
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(  
      ( params ) => {
        this.categoryId = params.get('categoryId')  
        if(this.categoryId){
          this.loadInfo(this.categoryId)
        }  
      }, 
      ( error ) => {

      } 
     )
  }

  actionStore(){
    let metodo = (this.categoryId) ? this.categoryService.update(this.categoryId, this.category) : this.categoryService.store(this.category);
    metodo.subscribe(
      () => { 
         this.router.navigate(['/categories'])
      },
      (error) => { 
        console.log(error) 
      }
    );
  } 

  loadInfo(categoryId:string){
    this.categoryService.get(categoryId).subscribe(
      (detail:any) => {  
         this.category = detail
      },
      (error) => { 
        console.log(error) 
      }
    );
  }


}
