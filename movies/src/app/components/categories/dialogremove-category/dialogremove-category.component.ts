import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Category } from '../../../interface/category';

@Component({
  selector: 'app-dialogremove-category',
  templateUrl: './dialogremove-category.component.html',
  styleUrls: ['./dialogremove-category.component.css']
})
export class DialogremoveCategoryComponent{
  actionOk = new EventEmitter();
  actionOkAll = new EventEmitter();
  constructor(
    public dialogRef: MatDialogRef<DialogremoveCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  
  onButtonClick(c:Category, deleteAll:boolean) {
    if(deleteAll)
      this.actionOkAll.emit(c);
    else
      this.actionOk.emit(c);
    
  } 

}

export interface DialogData { 
  category: any;
}
