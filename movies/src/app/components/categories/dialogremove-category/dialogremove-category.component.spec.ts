import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogremoveCategoryComponent } from './dialogremove-category.component';

describe('DialogremoveCategoryComponent', () => {
  let component: DialogremoveCategoryComponent;
  let fixture: ComponentFixture<DialogremoveCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogremoveCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogremoveCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
