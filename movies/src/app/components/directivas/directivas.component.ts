import { Component, OnInit } from '@angular/core';
//import { User } from '../../interface/user';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-directivas',
  templateUrl: './directivas.component.html',
  styleUrls: ['./directivas.component.css']
})
export class DirectivasComponent implements OnInit {

  texto:string;
  mostrar = true; 
  users: any[] //User[];

  constructor(
    private userServices: UserService
  ) {}

  ngOnInit() {

    this.texto = 'Lorem Ipsum'
 
   /* this.userServices.getListUser(1,10).then(
      (result: any) => { 
        this.users = result.data.map((user)=>({ 
          firstName: user.first_name,
          lastName: user.last_name,
          ...user
        }))
      },
      (error) => { 
        console.log(error)
        alert("Houston tenemos un problema")
      }
    );*/

    setTimeout( () => {
      this.texto = 'Bienvenidos'
    }, 2000)
  }

  ocultarMostrar = () => {
    this.mostrar = !this.mostrar;
  }

}
