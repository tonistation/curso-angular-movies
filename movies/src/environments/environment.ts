export const environment = {
  production: false,
  api_url: 'https://diplomado-restaurant-backend.herokuapp.com/',
  api_map: '192.168.1.3:3000',
  url_firebase: 'https://angularg13.firebaseio.com/',
  firebase: {
    apiKey: 'AIzaSyC-5bt7gQXSQ4y7_UfpsllG7T9lJBPycIc',
    authDomain: 'angularg13.firebaseapp.com',
    databaseURL: 'https://angularg13.firebaseio.com',
    projectId: 'angularg13',
    storageBucket: 'angularg13.appspot.com',
    messagingSenderId: '625671735029',
    appId: '1:625671735029:web:ba016dda4921c296',
    folderstore: 'avatar/',
    authDomainStorage: 'https://firebasestorage.googleapis.com/v0/b/',
  },
  rss_youtube: 'https://www.youtube.com/feeds/videos.xml?channel_id=',
  api_socket: 'http://localhost:8080',
  url_imdb : 'http://www.omdbapi.com/?apikey=5038aa82',
  photoURL: 'https://i7.pngguru.com/preview/393/995/701/aspria-fitness-computer-icons-user-clip-art-my-account-icon-thumbnail.jpg'
};