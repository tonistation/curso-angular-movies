const url = 'http://localhost';

let mensaje = 'Bienvenidos';
if ( true ) {
  let mensaje = 'Entro';
}
let mensaje2 = (true) ? 'Entro' : 'Bienvenidos';
// console.log(mensaje);

let numero: number;
let cadena: string;
let fecha : Date;
let booleanos: boolean;
let arrays : any[]

interface IUser {
  nombre: string,
  apellido: string,
  email: string
  hobbies?: string[]
}

let usuario: IUser;
let usuario2: IUser;

usuario = {
  nombre: '',
  apellido: '',
  email: '',
  hobbies: [],
}

usuario2 = {
  nombre: '',
  apellido: '',
  email: ''
}


// Funciones declaradas
function getNombre( 
  name:string, 
  lastname:string ='Carrion', 
  pais?:string
){
  console.log(`${name} ${lastname} ${ (pais) ? pais : 'Peru' }`);
}

getNombre('Kelvin', 'Castillo', 'Brasil');

// Funciones declaradas por expresion
const getLastname = (lastname: string) => {
  console.log(lastname);
}
// Funciones anonimas
(function(){
  console.log('Anonima')
})()

getLastname('Carrion');

// let nombre = '';
class Jugador {
  nombre: string = '';
  apodo = '';
  constructor(name: string, nickname: string){
    this.nombre = name;
    this.apodo = nickname;
  }

  getNickname() {
    return new Promise <string>( (resolve, reject) => {
      setTimeout( () => {
        return resolve(this.apodo);
      }, 2000);
    });
  }
}

const jugador1: Jugador = new Jugador('Paolo', 'El depredador');
// console.log( jugador1.getNickname() );
jugador1.getNickname().then(
  (info: string) => {
    console.log(info);
  },
  (err:any) => {
    console.log(err, 'Error');
  },
)