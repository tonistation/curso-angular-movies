"use strict";
const url = 'http://localhost';
let mensaje = 'Bienvenidos';
if (true) {
    let mensaje = 'Entro';
}
let mensaje2 = (true) ? 'Entro' : 'Bienvenidos';
// console.log(mensaje);
let numero;
let cadena;
let fecha;
let booleanos;
let arrays;
let usuario;
let usuario2;
usuario = {
    nombre: '',
    apellido: '',
    email: '',
    hobbies: [],
};
usuario2 = {
    nombre: '',
    apellido: '',
    email: ''
};
// Funciones declaradas
function getNombre(name, lastname = 'Carrion', pais) {
    console.log(`${name} ${lastname} ${(pais) ? pais : 'Peru'}`);
}
getNombre('Kelvin', 'Castillo', 'Brasil');
// Funciones declaradas por expresion
const getLastname = (lastname) => {
    console.log(lastname);
};
// Funciones anonimas
(function () {
    console.log('Anonima');
})();
getLastname('Carrion');
// let nombre = '';
class Jugador {
    constructor(name, nickname) {
        this.nombre = '';
        this.apodo = '';
        this.nombre = name;
        this.apodo = nickname;
    }
    getNickname() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                return resolve(this.apodo);
            }, 2000);
        });
    }
}
const jugador1 = new Jugador('Paolo', 'El depredador');
// console.log( jugador1.getNickname() );
jugador1.getNickname().then((info) => {
    console.log(info);
}, (err) => {
    console.log(err, 'Error');
});
